---
title: A Post with a Video
date: '2013-06-25 00:00:00'
categories: []
layout: post
description: Custom written post descriptions are the way to go... if you're not lazy.
tags:
- sample post
- video
slug: video-post
draft: false

---
<iframe width="560" height="315" src="//www.youtube.com/embed/SU3kYxJmWuQ" frameborder="0"></iframe>

Video embeds are responsive and scale with the width of the main content block with the help of [FitVids](http://fitvidsjs.com/).

```html
<iframe width="560" height="315" src="//www.youtube.com/embed/SU3kYxJmWuQ" frameborder="0"></iframe>
```